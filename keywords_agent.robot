*** Keywords ***
TC06-1
    login admin
    Maximize Browser Window
    Click Link    ${language-xpath}
    Click Link    ${mak-btn}
    Set Selenium Speed    ${speed-test}
    Click Link    ${click-agen}
    Set Selenium Speed    ${speed-test}
    Wait Until Page Contains    ${agent-page}
    Click Element    ${click-add-agen}
    Set Selenium Speed    ${speed-test}
    Input Text    ${agent-company-name}    TestRobot
    Input Text    ${agent-first-name}    Test
    Input Text    ${agent-last-name}    robot
    Input Text    ${agent-road}    1234
    Set Selenium Speed    ${speed-test}
    Click Element    ${save-agen-btn}
    Set Selenium Speed    ${speed-test}
    Close Browser

TC06-2
    login admin
    Maximize Browser Window
    Click Link    ${language-xpath}
    Click Link    ${mak-btn}
    Set Selenium Speed    ${speed-test}
    Click Link    ${click-agen}
    Set Selenium Speed    ${speed-test}
    Click Element    ${edit-agen-btn}
    Input Text    ${agent-company-name}    TestRobot
    Input Text    ${agent-first-name}    Test
    Input Text    ${agent-last-name}    robot
    Input Text    ${agent-road}    1234
    Set Selenium Speed    ${speed-test}
    Click Element    ${save-agen-btn}
    Set Selenium Speed    ${speed-test}
    Close Browser

TC06-3
    login admin
    Maximize Browser Window
    Click Link    ${language-xpath}
    Click Link    ${mak-btn}
    Set Selenium Speed    ${speed-test}
    Click Link    ${click-agen}
    Set Selenium Speed    ${speed-test}
    Click Element    ${delete-agen-btn}
    Set Selenium Speed    ${speed-test}
    Click Element    ${delete-agen-conf-btn}
    Set Selenium Speed    ${speed-test}
    Click Element    ${delete-agen-conf-ok-btn}
    Set Selenium Speed    ${speed-test}
    Close Browser