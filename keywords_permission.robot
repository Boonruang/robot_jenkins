*** Keywords ***

TC03-1
    login new tab
    Maximize Browser Window
    Click Link    ${user-btn}
    Set Selenium Speed    ${speed-test}
    Wait Until Page Contains    ${user-list-page}
    Click Element    ${edit-user-btn}
    Select From List    ${group-makler-btn}    ${maklerdatenbank-manager}
    Set Selenium Speed    ${speed-test}
    Click Button    ${save-user}
    Set Selenium Speed    ${speed-test}
    Select Window    url=${url-home}
    Set Selenium Speed    ${speed-test}
    Click Link    ${mak-btn}
    Set Selenium Speed    ${speed-test}
    Wait Until Page Contains    ${mkl-home-page}
    Click Element    ${click-offer-btn}
    Wait Until Page Contains    ${incompleted-page}
    Element Should Not Be Visible    ${btn-verify}
    Element Should Not Be Visible    ${btn-reject}
    Element Should Not Be Visible    ${btn-assign}
    Set Selenium Speed    ${speed-test}
    Select Window
    Click Element    ${edit-user-btn}
    Wait Until Page Contains    ${user-list-page}
    Select From List    ${group-makler-btn}    ${maklerdatenbank-leader}
    Set Selenium Speed    ${speed-test}
    Click Button    ${save-user}
    Close Browser