*** Keywords ***
TC04-1
    login admin
    Maximize Browser Window
    Click Link    ${language-xpath}
    Set Selenium Speed    ${speed-test}
    Click Link    ${user-btn}
    Set Selenium Speed    ${speed-test}
    Click Element    ${edit-user-btn}
    Select From List    ${group-user-btn}    ${usermanage-noaccess}
    Set Selenium Speed    ${speed-test}
    Click Button    ${save-user}
    Set Selenium Speed    ${speed-test}
    Wait Until Page Contains    ${success-save}
    Set Selenium Speed    ${speed-test}
    Wait Until Page Contains    ${user-list-page}
    Click Element    ${edit-user-btn}
    Select From List    ${group-user-btn}    ${usermanage-admin}
    Set Selenium Speed    ${speed-test}
    Click Button    ${save-user}
    Close Browser