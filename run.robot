*** Settings ***
Resource          settings.robot
Test Timeout    1 minutes
*** Variable ***
${directory-img}    img/
*** Test Cases ***

Test Login Facebook Success Case
    Open Browser    https://www.facebook.com    gc
    Set Window Size     1920    1080
    Input Text    id=email    xxx
    Input Text    id=pass    xxx
    Click Element    id=loginbutton
    Wait Until Page Contains    Aekachai Boonruang
    Capture Page Screenshot    ${directory-img}1.png
    Close Browser
Test Login Facebook Fail Case
    Open Browser    https://www.facebook.com    gc
    Set Window Size     1920    1080
    Input Text    id=email    oesife
    Input Text    id=pass    feoij
    Click Element    id=loginbutton
    Wait Until Page Contains    เข้าสู่ระบบ Facebook
    Capture Page Screenshot    ${directory-img}2.png
    Close Browser





# Test Open Procom Website
#       TC01-1

# Create Jenkins
#     Open Browser    http://localhost:8080/    gc
#     Input Text    id=j_username    Admin
#     Input Text    //*[@id="main-panel"]/div/form/table/tbody/tr[2]/td[2]/input    20scoops
#     Click Button    id=yui-gen1-button
#     Click Element    //*[@id="tasks"]/div[1]/a[2]
#     Input Text    //*[@id="name"]    testeee
#     Click Element    //*[@id="j-add-item-type-standalone-projects"]/ul/li[1]
#     Click Button    id=ok-button

# Test login (Normal Case)
#      TC02-1
# Test login (Duplicate Case)
#      TC02-2
# Test login (Sort Dashboard)
#      TC02-3
# Test login (Active menu)
#      TC02-4

# Test Permission
#      TC03-1

# Test User
#      TC04-1

# Test Makler 1 (Autofill Offer)
#      TC05-1
# Test Makler 2 (Not Complete Case)
#      TC05-2
# Test Makler 3 (Complete Case)
#      TC05-3
# Test Makler 4 (Reload Page After Save data)
#      TC05-4
# Test Makler 5 (Offer single page)
#      TC05-5
# Test Makler 6 (Node Detail)
#      TC05-6
# Test Makler 7 (List Offer)
#     TC05-7

# Test Agent 1 (Add Agent)
#      TC06-1
# Test Agent 2 (Edit Agent)
#      TC06-2
# Test Agent 3 (Delete Agent)
#      TC06-3
