*** Settings ***
Library           Selenium2Library
Library           ImageHorizonLibrary
Resource          variables.robot
Resource          keywords_setup.robot
Resource          keywords_openbrowser.robot
Resource          keywords_login.robot
Resource          keywords_add_offer.robot
Resource          keywords_permission.robot
Resource          keywords_user.robot
Resource          keywords_makler.robot
Resource          keywords_agent.robot
